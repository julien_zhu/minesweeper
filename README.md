# Minesweeper project

## Installation for Linux :
- For the UI, `ncurses` must be installed on your machine. If it's not, on Debian-based distributions like Ubuntu, on a terminal, type `sudo apt-get install ncurses`.
- Open a terminal in the folder, then type `make`.

## Usage :
- Launch the game by typing `./bin/minesweeper height width` where `height` and `width` are parameters of the grid, or `./bin/minesweeper mode` where `mode` is either : `EASY`, `NORMAL`, or `HARD`.
