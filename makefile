EXEC = minesweeper

CC = gcc
CFLAGS = -Wall -Wextra -std=c99 -g
LFLAGS = -lncurses

.PHONY : all clean debug

SRCDIR = src
HEADDIR = header
OBJDIR = obj
BINDIR = bin

SRC = $(wildcard $(SRCDIR)/*.c)
HDR = $(wildcard $(HEADDIR)/*.h)
OBJ = $(SRC:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

all: $(EXEC) $(SRC) $(HDR)

debug: all
debug: CFLAGS += -DDEBUG

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(HDR)
	mkdir -p $(OBJDIR)
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

$(EXEC): $(OBJ)
	mkdir -p $(BINDIR)
	$(CC) $(CFLAGS) $^ -o $(BINDIR)/$@ $(LFLAGS)
	make -s clean

clean:
	rm -rf $(OBJ)
