#ifndef __GAME_H__
#define __GAME_H__
/**
 * @file game.h
 * @brief Contains gameplay functions.
 * @date 2022-05-25
 */
#include <ncurses.h>
#include "board.h"

/**
 * @brief Recursive function. Reveals the cell by player's left click or by recursive propagation.
 * 
 * @param pb valid pointer to valid board
 * @param x 
 * @param y 
 * @param clic2 true if effect of clicking on already discovered tile
 */
void reveal_cell(board* pb, int x, int y/*, bool clic2*/);

void place_flag(board* pb, int x, int y);

void reveal_all(board* pb);

void test_game_over(WINDOW* w, board *pb);

#endif // _GAME_H__