#ifndef __MSG_H__
#define __MSG_H__
/**
 * @file msg.h
 * @brief Manages messages when playing
 * @date 2022-06-05
 */
#include <stdbool.h>
#include <ncurses.h>

typedef char* string;

typedef enum {
    LOST, //losing message
    WON, //winning message
    TITLE, //Minesweeper
    REMAINING, //Remaining bombs
    WAIT_CHOOSE,
} Msg;

//get the string version of Msg
string get_string(Msg msg);

/**
 * @brief prints message of msg
 * 
 * @param msg 
 * @return true if msg is known, false otherwise
 */
bool print_msg(WINDOW* w, Msg msg);

#endif //__MSG_H__