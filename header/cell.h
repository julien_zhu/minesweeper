#ifndef __CELL_H__
#define __CELL_H__
/**
 * @file cell.h
 * @brief Contains one tile information. Constructors, getters, setters. 
 * @date 2022-05-25
 */

#include <stdbool.h>

typedef struct _cell *cell;

// "Constructors" / "Destructors"
cell new_cell(int x, int y);
void free_cell(cell c);

// Setters
void set_neighbour(cell* pc, int neighbour);
void set_bomb(cell* pc, bool b);
void set_flag(cell* pc);
bool set_discovered(cell* pc); //return true if success, false if failure

// Getters
bool is_bomb(cell c);
bool is_flagged(cell c);
bool is_discovered(cell c);
int get_neighbour(cell c);
int get_x(cell c);
int get_y(cell c);

/**
 * @brief Checks if @b c and @b d share the same coordonites
 * 
 * @param c valid cell
 * @param d valid cell
 * @return true if @b c and @b d have same coordinates, false otherwise
 */
bool equals_coords(cell c, cell d);

#endif //__CELL_H__