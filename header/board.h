#ifndef __BOARD_H__
#define __BOARD_H__
/**
 * @file board.h
 * @brief Contains board structure (enhanced grid). Constructors, initializers and destructors. Getters and setters.
 * @date 2022-05-25
 */

#ifdef DEBUG
#define DENSITY_BOMB 0.05
#endif //DEBUG

#ifndef DEBUG
#define DENSITY_BOMB 0.16
#endif //nDEBUG

#include "grid.h"

typedef enum {
    EASY,
    NORMAL,
    HARD,
    CUSTOM
} Mode;

typedef struct _board* board;

board new_board(int width, int height, int n_bomb);
board new_board_by_mode(Mode mode);
void free_board(board b);

void set_gameover(board* pb);
void add_discovered(board* pb, int add);
void add_pf(board* pb, int n);
void set_pf(board* pb, int n);
void add_cpf(board* pb, int n);

bool is_gameover(board b);
grid get_grid(board b);
int get_width(board b);
int get_height(board b);
int get_pf(board b);
int get_cpf(board b);
int get_n_bomb(board b);
int get_discovered(board b);

// Initializing board objects : bombs then neighbours (number of bombs next to the cell)
void init_bomb(board *pb);
void init_neighbour(board *pb, int x, int y);
void init_all_neighbour(board *pb);


#endif //__BOARD_H__