#ifndef __IO_H__
#define __IO_H__
/**
 * @file io_terminal.h
 * @brief Contains printing functions for display.
 * @date 2022-05-25
 */
#include <ncurses.h>
#include "game.h"

typedef enum {
    LEFT_CLICK, //Discover cell
    RIGHT_CLICK, //Place/Remove flag
    QUIT_GAME,
} Input;

typedef struct {
    WINDOW* mainscr;
    WINDOW* board;
    WINDOW* input;
    WINDOW* msg;
}* Win;

//clear every line of window w
bool clearwin(WINDOW* w);

/**
 * @brief create the windows organization 
 * 
 * @param x nb of columns
 * @param y nb of lines
 * @return WINDOW* 
 */
Win init_window(int x, int y);

void end_window(Win win);

void print_board(Win win, board b);

/**
 * @brief Get the input from user
 * @param w valid initialized window
 * @param px valid pointer
 * @param py valid pointer
 * @return Input, places (x,y) coords in *px and *py
 */
Input get_input(Win win, int* px, int* py, board b);

#endif // __IO_H__