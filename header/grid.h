#ifndef __GRID_H__
#define __GRID_H__
/**
 * @file grid.h
 * @brief Contains grid structure (raw array of cells). Constructors, initializers and destructors.
 * @date 2022-05-25
 */
#include "cell.h"

typedef cell** grid;

grid new_grid(int width, int height);

void free_grid(grid g, int width, int height);


#endif //__GRID_H__