#include "../header/io.h"
#include "../header/msg.h"
#include "../header/game.h"

#define max(a, b) a>b ? a : b
#define min(a, b) a<b ? a : b

/* Return true if all cells neighbours to c are either discovered or flagged such that flagged_neighbours == neighbours(c)*/
// bool get_solved_cell(board *pb, cell c){
//     int flagged_neighbour = 0; //Counts flagged neighbours
//     int x = get_x(c), y = get_y(c);
//     int max_height = min(x+1, get_height(*pb)-1), max_width = min(y+1, get_width(*pb)-1);
//     //Observing the adjacent cells (except if boundary)
//     for (int j=max(y-1, 0); j<=max_height; j++){
//         for (int i=max(x-1, 0); i<=max_width; i++){
//             cell neighbour = (get_grid(*pb))[j][i];
//             flagged_neighbour += is_flagged(neighbour);
//         }
//     }
//     return (flagged_neighbour == get_neighbour(c));
// }

void reveal_cell(board* pb, int x, int y/*, bool clic2*/){
    cell c = get_grid(*pb)[y][x];
    if (is_discovered(c) || is_flagged(c))
        return;
    if (is_bomb(c)){
        set_gameover(pb);
        return;
    }
    add_discovered(pb, set_discovered(&c));  //sets_discovered, then adds to board numbers of cell dicsovered. bool is basically 0 or 1
    //Spreading
    if (get_neighbour(c) == 0){
        int max_height = min(y+1, get_height(*pb)-1), max_width = min(x+1, get_width(*pb)-1);
        //Observing the adjacent cells (except if boundary)
        for (int j = max(y-1, 0); j <= max_height; j++){
            for (int i = max(x-1, 0); i <= max_width; i++){
                reveal_cell(pb, i, j/*, false*/);
            }
        }
    }
}

void place_flag(board* pb, int x, int y){
    cell c = get_grid(*pb)[y][x];
    if (is_discovered(c))
        return;
    set_flag(&c);//Change status of flagged cell
    if (!is_flagged(c)){//Place flag if no flag
        add_pf(pb, -1);
        if (is_bomb(c))
            add_cpf(pb, -1);
        return;
    }//Remove flag if flag
    add_pf(pb, 1);
    if (is_bomb(c))
        add_cpf(pb, 1);
}

void reveal_all(board* pb){
    int height = get_height(*pb), width = get_width(*pb);
    for (int i=0; i<height; i++){
        for (int j=0; j<width; j++)
            add_discovered(pb, set_discovered(&(get_grid(*pb)[i][j]))); //sets_discovered, then adds to board numbers of cell dicsovered. bool is basically 0 or 1
    }
}

void test_game_over(WINDOW* w, board *pb){
    if (is_gameover(*pb)){
        clearwin(w);
        wmove(w, 0, 0);
            print_msg(w, LOST);
        wrefresh(w);
        reveal_all(pb);
        return;
    }
    if (!is_gameover(*pb) && (get_height(*pb)*get_width(*pb) == get_n_bomb(*pb) + get_discovered(*pb)) ){ //if all cells are either discovered or are bombs
        clearwin(w);
        wmove(w, 0, 0);
            print_msg(w, WON);
        wrefresh(w);
        set_pf(pb, get_n_bomb(*pb)); //sets number of placed flags as on every bomb cells ==> number of remaining bombs = 0 in display
        reveal_all(pb);
        set_gameover(pb);
    }
}