#include <ncurses.h>
#include "../header/msg.h"

string get_string(Msg msg){
    switch (msg){
        case LOST:
            return "You lost. Press any key.";
        case WON:
            return "You won. Press any key.";
        case TITLE:
            return "MINESWEEPER";
        case REMAINING:
            return "Remaining bombs";
        case WAIT_CHOOSE:
            return "Choose your cell using arrow keys.\nPress spacebar to discover cell\nor press F to place/remove flag.\nPress q to quit.";
        default:
            return "Unknown message.";
    }
}

bool print_msg(WINDOW* w, Msg msg){
    string mstr = get_string(msg);
    wprintw(w, "%s", mstr);
    return true;
}