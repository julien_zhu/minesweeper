#include <stdlib.h>
#include "../header/cell.h"

struct _cell {
    int neighbour; //Number of neighbours who have bomb
    int x; //x-coord : width index
    int y; //y-coord : height index
    bool bomb; //has bomb ?
    bool flagged; // was flagged ?
    bool discovered; //was discovered ?
};

cell new_cell(int x, int y){
    cell out = malloc(sizeof *out);
        out->neighbour = 0;
        out->x = x;
        out->y = y;
        out->bomb = false;
        out->flagged = false;
        out->discovered = false;
    return out;
}

void set_neighbour(cell* pc, int neighbour){(*pc)->neighbour = neighbour;}
void set_bomb(cell* pc, bool b){(*pc)->bomb = b;}

void set_flag(cell* pc){
    if (!(*pc)->discovered)
        (*pc)->flagged = !((*pc)->flagged);
}

bool set_discovered(cell* pc){
    if (!(*pc)->discovered){
        (*pc)->discovered = true;
        return true;
    }
    return false;
}

bool is_bomb(cell c){return c->bomb;}
bool is_flagged(cell c){return c->flagged;}
bool is_discovered(cell c){return c->discovered;}
int get_neighbour(cell c){return c->neighbour;}
int get_x(cell c){return c->x;}
int get_y(cell c){return c->y;}

void free_cell(cell c){
    free(c);
}

bool equals_coords(cell c, cell d){
    return (c->x == d->x && c->y == d->y);
}