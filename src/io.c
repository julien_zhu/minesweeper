#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include "../header/io.h"
#include "../header/msg.h"

#define MAX_INPUT 50

/**
 * @brief print @b s to the center of line @b y in @b w
 * 
 * @param w window
 * @param y height/number of line
 * @param s string to print
 */
void print_center(WINDOW* w, int y, char* s){
    wmove(w, y, 1);
    for (int i=1; i<COLS-1; i++){
        wprintw(w, " ");
    }
    mvwprintw(w, y, (COLS - strlen(s))/2, s);
}

bool clearwin(WINDOW* w){
    for (int y=0; y<getmaxy(w); y++){
        wmove(w, y, 0);
        wclrtoeol(w);
    }
    return true;
}

Win init_window(int x, int y){
    Win out = malloc(sizeof *out);
        out->mainscr = initscr();
            box(out->mainscr, ACS_VLINE, ACS_HLINE);
        out->board = subwin(stdscr, x+2, 3*y+2, 3, (COLS - 3*y)/2 -1 );
            box(out->board, ACS_VLINE, ACS_HLINE);
        out->input = subwin(stdscr, 1, 1, 1, 1);
            wattron(out->input, A_INVIS);                           //Input from user are invisible
        out->msg = subwin(stdscr, 4, 35, LINES-5, (COLS - 35)/2 );
    //Gameplay message
    wprintw(out->msg, "%s", get_string(WAIT_CHOOSE));
    wrefresh(out->msg);

    print_center(out->mainscr, 1, get_string(TITLE));               //Print centered title
    return out;
}

void end_window(Win win){
    //Waiting for user to quit
    wgetch(win->input);
    free(win);
    endwin();
}

void print_cell_val(Win win, cell c){
    WINDOW* bwin = win->board;
    int x = get_x(c), y = get_y(c);
    if (is_flagged(c)){
        if (is_discovered(c))
            mvwaddch(bwin, y+1, 3*x+2, is_bomb(c)? 'F' : 'X'|A_BLINK); //🚩 no standout if rightly placed flag. blinking if wrongly placed.
        else
            mvwaddch(bwin, y+1, 3*x+2, 'F'|A_DIM); //🚩
        return;
    }
    if (!is_discovered(c)){
        mvwaddch(bwin, y+1, 3*x+2, '.'|A_DIM); //🟦
        return;
    }
    if (is_bomb(c)){
        mvwaddch(bwin, y+1, 3*x+2, '*'); //💣 B
        return;
    }
    if (get_neighbour(c) == 0){
        mvwaddch(bwin, y+1, 3*x+2, ' ');
        return;
    }
    mvwaddch(bwin, y+1, 3*x+2, (get_neighbour(c) + '0')|A_BOLD);
}

void print_grid(Win win, grid g, int w, int h){
    for (int i=0; i<h; i++)
        for (int j=0; j<w; j++)
            print_cell_val(win, g[i][j]);
}

void print_board(Win win, board b){
    //Printing remaining bombs
    char* line = malloc(MAX_INPUT*sizeof(char));
        sprintf(line, "%s : %d", get_string(REMAINING), get_n_bomb(b) - get_pf(b));
    print_center(win->mainscr, 2, line);
    wrefresh(win->mainscr);
    //Print grid
    print_grid(win, get_grid(b), get_width(b), get_height(b));
    wrefresh(win->board);
}

Input get_input(Win win, int* px, int* py, board b){
    grid g = get_grid(b);

    //Initialize cell highlight
    wattron(win->board, A_REVERSE | A_DIM);
    print_cell_val(win, get_grid(b)[*py][*px]); //Update cell to put the window attributes
    wrefresh(win->board);

    int scan_input;
    while (scan_input = wgetch(win->input), scan_input != 'f' && scan_input != ' ' && scan_input != 'q'){
        //Turn off previous cell
        wattroff(win->board, A_REVERSE | A_DIM);
        print_cell_val(win, g[*py][*px]);

        //Scanning value
        switch (scan_input) {
            case KEY_UP:
                if ((*py) > 0) (*py)--;
                break;
            case KEY_DOWN:
                if (*py < get_height(b) - 1) (*py)++;
                break;
            case KEY_LEFT:
                if (*px > 0) (*px)--;
                break;
            case KEY_RIGHT:
                if (*px < get_width(b) - 1) (*px)++;
                break;
            default:
                break;
        }
        //Turn on selected cell
        wattron(win->board, A_REVERSE | A_DIM);
        print_cell_val(win, g[*py][*px]); //Update cell to put the window attributes
        wrefresh(win->board);
    }
    //Turn off cell for the rest of the program
    wattroff(win->board, A_REVERSE | A_DIM);

    switch (scan_input) {
        case 'f':
            return RIGHT_CLICK;
        case ' ':
            return LEFT_CLICK;
        case 'q':
            set_gameover(&b);
            return QUIT_GAME;
        default: //Should never happen
            return LEFT_CLICK;
    }
}