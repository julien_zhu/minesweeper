#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "../header/io.h"

int main(int argc, char* argv[]){
    #ifndef DEBUG
    srand(time(NULL));
    #endif //nDEBUG
    if (argc!=3 && argc !=2){
        fprintf(stderr, "Usage : %s height width OR %s [EASY|NORMAL|HARD]\n", argv[0], argv[0]);
        return EXIT_FAILURE;
    }
    board b;
    switch (argc) {
    case 2:{
        Mode mode;
        char* mode_string = argv[1];
        if (strcmp(mode_string, "EASY") == 0)
            mode = EASY;
        else if (strcmp(mode_string, "NORMAL") == 0)
            mode = NORMAL;
        else if (strcmp(mode_string, "HARD") == 0)
            mode = HARD;
        else {
            fprintf(stderr, "%s is not valid type. Valid types are : \"EASY\", \"NORMAL\", \"HARD\" \n", argv[1]);
            return EXIT_FAILURE;
        }
        b = new_board_by_mode(mode);
        break;
    }
    case 3:{
        int height = atoi(argv[1]), width = atoi(argv[2]);
        int n_bomb = (int)(width*height*DENSITY_BOMB);
        b = new_board(width, height, n_bomb);
        break;
    }
    default:
        b = new_board(10, 10, (int)(10*10*DENSITY_BOMB));
        break;
    }
    
    Win win = init_window(get_height(b), get_width(b));
    //cursors
    keypad(win->input, TRUE); //allows multiple characters like arrows
    curs_set(0); //hide cursor
    //test if window is too small
    if (LINES < 10+get_height(b)){
        endwin();
        free(win);
        fprintf(stderr, "Terminal is too small. Height : %d, Required height : %d\n", LINES, 10+get_height(b));
        return EXIT_FAILURE;
    }
    if (COLS < 3*get_width(b) +4){
        endwin();
        free(win);
        fprintf(stderr, "Terminal is too small. Width : %d, Required width : %d\n", COLS, 3*get_width(b) +4);
        return EXIT_FAILURE;
    }
    print_board(win, b);
    int x = 0, y = 0; //Coordinates of the chosen cell
    while(!is_gameover(b)){
        Input input = get_input(win, &x, &y, b);
        switch (input) {
        case LEFT_CLICK: //discovering
            reveal_cell(&b, x, y/*, false*/);
            break;
        case RIGHT_CLICK: //placing flag
            place_flag(&b, x, y);
            break;
        case QUIT_GAME:
            free_board(b);
            free(win);
            endwin();
            printf("Game quitted.\n");
            return EXIT_SUCCESS;
            break;
        default:
            break;
        }
        test_game_over(win->msg, &b);
        print_board(win, b);
    }
    free_board(b);
    end_window(win);
    return EXIT_SUCCESS;
}