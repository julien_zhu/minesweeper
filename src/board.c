#include <stdlib.h>
#include "../header/board.h"

#define max(a, b) a>b ? a : b
#define min(a, b) a<b ? a : b

struct _board {
    grid g;
    int width;
    int height;
    int n_bomb;
    int pf; //placed flags
    int cpf; //correctly placed flags
    int discovered; //number of discovered cells
    bool gameover;
};

board new_board(int width, int height, int n_bomb){
    board out = malloc(sizeof *out);
        out->g = new_grid(width, height);
        out->width = width;
        out->height = height;
        out->n_bomb = n_bomb;
        out->pf = 0;
        out->cpf = 0;
        out->discovered = 0;
        out->gameover = false;
    init_bomb(&out);
    init_all_neighbour(&out);
    return out;
}

board new_board_by_mode(Mode mode){
    switch (mode) {
        case EASY: return new_board(8, 8, 8*8*DENSITY_BOMB);
        case NORMAL: return new_board(16, 16, 16*16*DENSITY_BOMB);
        case HARD: return new_board(30, 16, 16*30*DENSITY_BOMB);
        default: return new_board(8, 8, 8*8*DENSITY_BOMB);
    }
}

void free_board(board b){
    free_grid(b->g, b->width, b->height);
    free(b);
}

void set_gameover(board* pb){(*pb)->gameover = true;}

void add_discovered(board* pb, int add){
    (*pb)->discovered += add;
}

void add_pf(board* pb, int n){
    (*pb)->pf = get_pf(*pb) + n;
}

void set_pf(board* pb, int n){
    (*pb)->pf = n;
}

void add_cpf(board* pb, int n){
    (*pb)->cpf = get_cpf(*pb) + n;
}

bool is_gameover(board b){return b->gameover;}
grid get_grid(board b){return b->g;}
int get_height(board b){return b->height;}
int get_width(board b){return b->width;}
int get_pf(board b){return b->pf;}
int get_cpf(board b){return b->cpf;}
int get_n_bomb(board b){return b->n_bomb;}
int get_discovered(board b){return b->discovered;}

// Test if cell already chosen in array C of size size
bool mem(cell c, cell C[], size_t size){
    for (long unsigned int i=0; i<size; i++)
        if (equals_coords(c, C[i]))
            return true;
    return false; //not found
}

void init_bomb(board *pb){
    int n_bomb = (*pb)->n_bomb;
    int height = (*pb)->height, width = (*pb)->width;
    //array of chosen cells
    cell bomb_list[n_bomb];
    int i=0;
    while (i<n_bomb){
        //Choosing cells coordinates
        int x = rand()%(height), y = rand()%(width);
        cell c = ((*pb)->g)[x][y];
        //Not choosing already chosen cells
        while (mem(c, bomb_list, i)){
            x = rand()%(height);
            y = rand()%(width);
            c = ((*pb)->g)[x][y];
        }
        bomb_list[i] = c;
        //Making it bomb cell
        set_bomb(&c, true);

        i++;
    }
}

void init_neighbour(board *pb, int x, int y){
    int n = 0;
    int max_height = min(x+1, get_height(*pb)-1);
    int max_width = min(y+1, get_width(*pb)-1);
    //Observing the adjacent cells (except if boundary)
    for (int i = max(x-1, 0); i <= max_height; i++)
        for (int j = max(y-1, 0); j <= max_width; j++)
            n += is_bomb(((*pb)->g)[i][j]);
    set_neighbour(&((*pb)->g[x][y]), n);
}

void init_all_neighbour(board *pb){
    int height = get_height(*pb);
    int width = get_width(*pb);
    for (int i=0; i<height; i++)
        for (int j=0; j<width; j++)
            init_neighbour(pb, i, j);
}