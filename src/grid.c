#include <stdlib.h>
#include "../header/grid.h"

grid new_grid(int width, int height){
    grid out = malloc (height * sizeof(cell*));
    for (int j=0; j<height; j++){
        cell* lin = malloc (width * sizeof(cell));
        for (int i=0; i<width; i++)
            lin[i] = new_cell(i, j);
        out[j] = lin;
    }
    return out;
}

void free_grid(grid g, int width, int height){
    for (int j=0; j<height; j++){
        for (int i=0; i<width; i++)
            free_cell(g[j][i]);
        free(g[j]);
    }
    free(g);
}
